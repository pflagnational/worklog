require 'spec_helper'

describe "leave_balances/show" do
  before(:each) do
    @leave_balance = assign(:leave_balance, stub_model(LeaveBalance,
      :category => "Category",
      :balance => "9.99"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Category/)
    rendered.should match(/9.99/)
  end
end
