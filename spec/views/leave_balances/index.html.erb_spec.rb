require 'spec_helper'

describe "leave_balances/index" do
  before(:each) do
    assign(:leave_balances, [
      stub_model(LeaveBalance,
        :category => "Category",
        :balance => "9.99"
      ),
      stub_model(LeaveBalance,
        :category => "Category",
        :balance => "9.99"
      )
    ])
  end

  it "renders a list of leave_balances" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Category".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
  end
end
