require 'spec_helper'

describe "leave_balances/edit" do
  before(:each) do
    @leave_balance = assign(:leave_balance, stub_model(LeaveBalance,
      :category => "MyString",
      :balance => "9.99"
    ))
  end

  it "renders the edit leave_balance form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => leave_balances_path(@leave_balance), :method => "post" do
      assert_select "input#leave_balance_category", :name => "leave_balance[category]"
      assert_select "input#leave_balance_balance", :name => "leave_balance[balance]"
    end
  end
end
