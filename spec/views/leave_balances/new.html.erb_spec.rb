require 'spec_helper'

describe "leave_balances/new" do
  before(:each) do
    assign(:leave_balance, stub_model(LeaveBalance,
      :category => "MyString",
      :balance => "9.99"
    ).as_new_record)
  end

  it "renders new leave_balance form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => leave_balances_path, :method => "post" do
      assert_select "input#leave_balance_category", :name => "leave_balance[category]"
      assert_select "input#leave_balance_balance", :name => "leave_balance[balance]"
    end
  end
end
