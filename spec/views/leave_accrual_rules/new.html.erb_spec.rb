require 'spec_helper'

describe "leave_accrual_rules/new" do
  before(:each) do
    assign(:leave_accrual_rule, stub_model(LeaveAccrualRule,
      :accrual_type => "MyString",
      :leave_code => "MyString",
      :condition_type => "MyString",
      :condition_unit => "9.99",
      :unit => "9.99",
      :reset_type => "MyString",
      :reset_frequency => "MyString",
      :reset_min => "9.99",
      :reset_max => "9.99",
      :reset_max_per_year_of_service => "9.99"
    ).as_new_record)
  end

  it "renders new leave_accrual_rule form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => leave_accrual_rules_path, :method => "post" do
      assert_select "input#leave_accrual_rule_accrual_type", :name => "leave_accrual_rule[accrual_type]"
      assert_select "input#leave_accrual_rule_leave_code", :name => "leave_accrual_rule[leave_code]"
      assert_select "input#leave_accrual_rule_condition_type", :name => "leave_accrual_rule[condition_type]"
      assert_select "input#leave_accrual_rule_condition_unit", :name => "leave_accrual_rule[condition_unit]"
      assert_select "input#leave_accrual_rule_unit", :name => "leave_accrual_rule[unit]"
      assert_select "input#leave_accrual_rule_reset_type", :name => "leave_accrual_rule[reset_type]"
      assert_select "input#leave_accrual_rule_reset_frequency", :name => "leave_accrual_rule[reset_frequency]"
      assert_select "input#leave_accrual_rule_reset_min", :name => "leave_accrual_rule[reset_min]"
      assert_select "input#leave_accrual_rule_reset_max", :name => "leave_accrual_rule[reset_max]"
      assert_select "input#leave_accrual_rule_reset_max_per_year_of_service", :name => "leave_accrual_rule[reset_max_per_year_of_service]"
    end
  end
end
