require 'spec_helper'

describe "leave_accrual_rules/index" do
  before(:each) do
    assign(:leave_accrual_rules, [
      stub_model(LeaveAccrualRule,
        :accrual_type => "Accrual Type",
        :leave_code => "Leave Code",
        :condition_type => "Condition Type",
        :condition_unit => "9.99",
        :unit => "9.99",
        :reset_type => "Reset Type",
        :reset_frequency => "Reset Frequency",
        :reset_min => "9.99",
        :reset_max => "9.99",
        :reset_max_per_year_of_service => "9.99"
      ),
      stub_model(LeaveAccrualRule,
        :accrual_type => "Accrual Type",
        :leave_code => "Leave Code",
        :condition_type => "Condition Type",
        :condition_unit => "9.99",
        :unit => "9.99",
        :reset_type => "Reset Type",
        :reset_frequency => "Reset Frequency",
        :reset_min => "9.99",
        :reset_max => "9.99",
        :reset_max_per_year_of_service => "9.99"
      )
    ])
  end

  it "renders a list of leave_accrual_rules" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Accrual Type".to_s, :count => 2
    assert_select "tr>td", :text => "Leave Code".to_s, :count => 2
    assert_select "tr>td", :text => "Condition Type".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "Reset Type".to_s, :count => 2
    assert_select "tr>td", :text => "Reset Frequency".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
  end
end
