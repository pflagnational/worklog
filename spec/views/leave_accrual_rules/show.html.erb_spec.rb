require 'spec_helper'

describe "leave_accrual_rules/show" do
  before(:each) do
    @leave_accrual_rule = assign(:leave_accrual_rule, stub_model(LeaveAccrualRule,
      :accrual_type => "Accrual Type",
      :leave_code => "Leave Code",
      :condition_type => "Condition Type",
      :condition_unit => "9.99",
      :unit => "9.99",
      :reset_type => "Reset Type",
      :reset_frequency => "Reset Frequency",
      :reset_min => "9.99",
      :reset_max => "9.99",
      :reset_max_per_year_of_service => "9.99"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Accrual Type/)
    rendered.should match(/Leave Code/)
    rendered.should match(/Condition Type/)
    rendered.should match(/9.99/)
    rendered.should match(/9.99/)
    rendered.should match(/Reset Type/)
    rendered.should match(/Reset Frequency/)
    rendered.should match(/9.99/)
    rendered.should match(/9.99/)
    rendered.should match(/9.99/)
  end
end
