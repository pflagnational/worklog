require 'spec_helper'

describe "codes/show" do
  before(:each) do
    @code = assign(:code, stub_model(Code,
      :category => "Category",
      :code => "Code",
      :description => "Description"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Category/)
    rendered.should match(/Code/)
    rendered.should match(/Description/)
  end
end
