require "spec_helper"

describe LeaveAccrualRulesController do
  describe "routing" do

    it "routes to #index" do
      get("/leave_accrual_rules").should route_to("leave_accrual_rules#index")
    end

    it "routes to #new" do
      get("/leave_accrual_rules/new").should route_to("leave_accrual_rules#new")
    end

    it "routes to #show" do
      get("/leave_accrual_rules/1").should route_to("leave_accrual_rules#show", :id => "1")
    end

    it "routes to #edit" do
      get("/leave_accrual_rules/1/edit").should route_to("leave_accrual_rules#edit", :id => "1")
    end

    it "routes to #create" do
      post("/leave_accrual_rules").should route_to("leave_accrual_rules#create")
    end

    it "routes to #update" do
      put("/leave_accrual_rules/1").should route_to("leave_accrual_rules#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/leave_accrual_rules/1").should route_to("leave_accrual_rules#destroy", :id => "1")
    end

  end
end
