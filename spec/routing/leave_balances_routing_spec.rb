require "spec_helper"

describe LeaveBalancesController do
  describe "routing" do

    it "routes to #index" do
      get("/leave_balances").should route_to("leave_balances#index")
    end

    it "routes to #new" do
      get("/leave_balances/new").should route_to("leave_balances#new")
    end

    it "routes to #show" do
      get("/leave_balances/1").should route_to("leave_balances#show", :id => "1")
    end

    it "routes to #edit" do
      get("/leave_balances/1/edit").should route_to("leave_balances#edit", :id => "1")
    end

    it "routes to #create" do
      post("/leave_balances").should route_to("leave_balances#create")
    end

    it "routes to #update" do
      put("/leave_balances/1").should route_to("leave_balances#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/leave_balances/1").should route_to("leave_balances#destroy", :id => "1")
    end

  end
end
