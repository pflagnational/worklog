# == Schema Information
#
# Table name: leave_accrual_rules
#
#  id                            :integer          not null, primary key
#  accrual_type                  :string(255)
#  leave_code                    :string(255)
#  condition_type                :string(255)
#  condition_unit                :decimal(, )
#  unit                          :decimal(, )
#  reset_type                    :string(255)
#  reset_frequency               :string(255)
#  reset_date                    :date
#  reset_min                     :decimal(, )
#  reset_max                     :decimal(, )
#  reset_max_per_year_of_service :decimal(, )
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  accrual_frequency             :string(255)
#

require 'spec_helper'

describe LeaveAccrualRule do
  pending "add some examples to (or delete) #{__FILE__}"
end
