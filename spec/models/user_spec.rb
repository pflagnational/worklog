# == Schema Information
#
# Table name: users
#
#  id                :integer          not null, primary key
#  provider          :string(255)
#  uid               :string(255)
#  email             :string(255)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  adp_id            :integer
#  department_id     :integer
#  users             :string(255)
#  first_name        :string(255)
#  last_name         :string(255)
#  supervisor_id     :integer
#  hire_date         :date
#  accrual_type      :string(255)
#  alias_for_user_id :integer          default(-1)
#  password_digest   :string(255)
#

require 'spec_helper'

describe User do
  pending "add some examples to (or delete) #{__FILE__}"
end
