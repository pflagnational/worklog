Worklogs::Application.routes.draw do
  break if ARGV.join.include? 'assets:precompile'



  resources :leave_accrual_rules
  resources :leave_balances do
    collection { post :import }
  end

  resources :codes do
    collection do
      post :import
    end
    collection do
      post :delete_multiple
    end

  end

  resources :hours
  resources :users
  match '/worklogs/template', :to => 'worklogs#template', :as => :work_log_template

  resources :worklogs do
    collection do
      post :delete_multiple
    end
    collection do
      post :approve_multiple
    end
  end
    
  resources :bulk_work_log_updates, :only => [:create] do
    collection do
      post :update
    end
  end
    
  resources :sessions, :only => [:new, :create, :destroy]

  get "pages/home"
  get "pages/help"


  match 'auth/:provider/callback', to: 'sessions#create'

  match 'auth/failure', to: redirect('/')
  match 'signout', to: 'sessions#destroy', as: 'signout'
  match 'signin', to: redirect('/auth/google_oauth2'), as: 'signin'
  match '/help',    :to => 'pages#help'
  match '/home',    :to => 'pages#home'
  match '/worklogs/:id/approve', :to => 'worklogs#approve', :as => :approve_work_log
  match '/worklogs/:id/reject', :to => 'worklogs#reject', :as => :reject_work_log
  match '/worklogs/new', :to => 'worklogs#new', :as => :new_work_logs
  match '/worklogs/', :to => 'worklogs#index', :as => :work_logs
  match '/worklogs/:id', :to => 'worklogs#show', :as => :work_log
  match '/worklogs/:id/edit', :to => 'worklogs#edit', :as => :edit_work_log
  root :to => 'worklogs#new'

end
