require File.expand_path('../ssl_config', __FILE__)

# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
Worklogs::Application.initialize!
