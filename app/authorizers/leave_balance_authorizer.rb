class LeaveBalanceAuthorizer < ApplicationAuthorizer
  def self.creatable_by?(user)
    user.has_role? :admin
  end

  def self.readable_by?(user)
    user.has_role? :employee
  end
    
  def readable_by?(user)
    resource.user == user or user.has_role? :admin
      #resource.submit_to == user.id or resource.supervisor_id == user.id 
  end
  def self.updatable_by?(user)
    user.has_role? :admin
  end
  def updatable_by?(user)
    user.has_role? :admin
  end
  def deletable_by?(user)
    user.has_role? :admin
  end
end
