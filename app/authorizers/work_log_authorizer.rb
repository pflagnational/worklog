class WorkLogAuthorizer < ApplicationAuthorizer
  def self.creatable_by?(user)
    user.has_any_role? :employee, :supervisor, :admin
  end

  def self.readable_by?(user)
    user.has_role? :employee
  end
    
  def readable_by?(user)
    resource.user == user or resource.submit_to == user.id or resource.user.supervisor == user or user.has_role? :admin
  end
  def self.updatable_by?(user)
    user.has_role? :employee
  end
  def self.approvable_by?(user)
    user.has_any_role? :supervisor, :admin
  end
  def self.rejectable_by?(user)
    user.has_any_role? :supervisor, :admin
  end
    
  def self.deletable_by?(user)
      user.has_any_role? :employee, :admin
  end   
    
  def updatable_by?(user)
      #only allow edit if not approved nor processed
      !resource.approved? and !resource.processed? and
          authorized_user(user)
  end
  def approvable_by?(user)
      #only allow approval if not approved and supervisor, submitted to alternate supervisor or admin
    !resource.approved? and authorized_approver(user)
  end
  def rejectable_by?(user)
    #only allow reject if not approved nor processed and supervisor, submitted to alternate supervisor or admin
    !resource.processed? and authorized_approver(user)
  end
  def deletable_by?(user)
    #only allow delete if not processed and user, approver, supervisor or admin
    !resource.processed? and !resource.approved? and authorized_approver(user)
  end
    
  def processable_by?(user)
    user.has_role? :admin
  end
    
    private
      def authorized_user(user) 
          if (resource.user.nil?) then 
              user.has_role? :admin
          else
              user.id.in?([resource.user.id, resource.submit_to, resource.approved_by_id, (resource.user.supervisor.id unless resource.user.supervisor.nil?)]) or user.has_role? :admin
          end
      end
      def authorized_approver(user)
          if (resource.user.nil?) then 
              user.has_role? :admin
          else
              user.id.in?([resource.submit_to, resource.approved_by_id, (resource.user.supervisor.id unless resource.user.supervisor.nil?)]) or user.has_role? :admin
          end
      end
end
