class UserMailer < ActionMailer::Base
  helper :mail
  default from: "\"Work Logs\" <info@pflag.org>"
  def approval_email(user, supervisor, work_log, new = false)
    @user = user
    @supervisor = supervisor
    @work_log = work_log
    @work_log_user = @work_log.user
      cc = ["\"#{user.first_name} #{user.last_name}\" <#{user.email}>"]
      cc.push("\"#{@work_log_user.first_name} #{@work_log_user.last_name}\" <#{@work_log_user.email}>") unless (@user == @work_log.user || @work_log.user.nil?) 
    state = (new) ? "New" : "Updated"
    subject = "#{state} Work Log from #{@work_log.user.full_name}"
    mail(:to => supervisor.email, :cc => cc, :subject => subject)
  end
  def reject_email(user, supervisor, work_log, new = false)
    @user = user
    @supervisor = supervisor
    @work_log = work_log
    state = (new) ? "New" : "Updated"
    subject = "Work Log Rejected!"
    mail(:to => work_log.user.email, :cc => user.email, :subject => subject)
  end
  def new_user_email(user)
    @user = user
    subject = "New User Created"
    mail(:to => User.administrators.map(&:email), :subject => subject)
  end

  def code_style(code)
    if code == "CE"
      style = " style=\"color: green;\""
    else
      style = ""
    end
    style
  end

end
