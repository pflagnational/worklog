class LeaveBalancesController < ApplicationController
  before_filter :authenticate
  before_filter :require_admin, :except => [:index, :show]
    authorize_actions_for LeaveBalance
    # GET /leave_balances.json
  def index
    @leave_balances = current_user.direct_reports_leave_balances.filter(filtering_params(params)).order(:user_id)
    @date = Date.parse(params[:pay_period]) unless params[:pay_period].blank?

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @leave_balances.to_json(:pay_period => @date)}
    end
  end

  # GET /leave_balances/1
  # GET /leave_balances/1.json
  def show
    @leave_balance = LeaveBalance.find(params[:id])
      authorize_action_for(@leave_balance)
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @leave_balance }
    end
  end

  # GET /leave_balances/new
  # GET /leave_balances/new.json
  def new
    @leave_balance = LeaveBalance.new
    @leave_balance.uploaded = Date.today

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @leave_balance }
    end
  end

  # GET /leave_balances/1/edit
  def edit
    @leave_balance = LeaveBalance.find(params[:id])
  end

  # POST /leave_balances
  # POST /leave_balances.json
  def create
    @leave_balance = LeaveBalance.new(params[:leave_balance])

    respond_to do |format|
      if @leave_balance.save
        format.html { redirect_to @leave_balance, notice: 'Leave balance was successfully created.' }
        format.json { render json: @leave_balance, status: :created, location: @leave_balance }
      else
        format.html { render action: "new" }
        format.json { render json: @leave_balance.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /leave_balances/1
  # PUT /leave_balances/1.json
  def update
    @leave_balance = LeaveBalance.find(params[:id])
    authorize_action_for(@leave_balance)

    respond_to do |format|
      if @leave_balance.update_attributes(params[:leave_balance])
        format.html { redirect_to leave_balances_url, notice: 'Leave balance was successfully updated.' }
        format.json { head :no_content }
      else
        flash[:error] = @leave_balance.errors.full_messages.to_sentence
        format.html { render action: "edit" }
        format.json { render json: @leave_balance.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /leave_balances/1
  # DELETE /leave_balances/1.json
  def destroy
    @leave_balance = LeaveBalance.find(params[:id])
    authorize_action_for(@leave_balance)
    @leave_balance.destroy

    respond_to do |format|
      format.html { redirect_to leave_balances_url }
      format.json { head :no_content }
    end
  end

  def import
    errors = LeaveBalance.import(params[:file], params[:uploaded] || WorkLog.last_valid_pay_period)
    message =  (errors.length > 0) ? errors.join(", ") : "Leave balances imported."
    redirect_to leave_balances_url, notice: message 
    
  end
    private

    # A list of the param names that can be used for filtering
    def filtering_params(params)
      params.slice(:user_id)
    end

end
