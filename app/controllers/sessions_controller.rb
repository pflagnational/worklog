class SessionsController < ApplicationController

  def create
    begin
      user = User.from_omniauth(env["omniauth.auth"])
    rescue UserDomainError => e
      flash[:error] = e.message
    end

    if user
      session[:user_id] = user.id
  	  redirect_back_or new_worklog_path, notice: "Signed in!"
    else
      redirect_to home_path, error: "Login failed!"
    end

  end
  def destroy
      session[:user_id] = nil
      redirect_to home_path, notice: "Signed out!"
  end
end