class UsersController < ApplicationController
  before_filter :require_admin

  def new
    @title = "Create user"
    @user = User.new(:hire_date => Date.today(), :provider => "google_oauth2")
  end

  def create
    
    @user = User.new(params[:user])
    @user.provider = "google_oauth2" if @user.provider.blank?

    if @user.save
      redirect_to users_path, :flash => { :success => "User Created!" }
    else
      @title = "Create user"
      render 'new'
    end
  end
  def edit
    @user = User.find(params[:id])
  end
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(params[:user])
      redirect_to users_path, :flash => { :success => 'User saved'}
    else
      @title = 'Edit User'
      render 'edit'
    end
  end

  def index
    @users = User.all

    respond_to do |format|
      format.html  # index.html.erb
      format.xml  { render :xml => @users}
      format.json  { render :json => @users }
    end
  end
  def show
    @user = User.find(params[:id])
  end

  def destroy
    @user = User.find(params[:id])
    if @user.destroy
      redirect_to @user, :flash => { :success => "User Deleted." }
    else
      redirect_to @user, :flash => { :error => "User Delete Failed." }
      
    end
  end
end
