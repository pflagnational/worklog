class WorklogsController < ApplicationController
  before_filter :authenticate
  authorize_actions_for WorkLog, :new => :create, :actions => {:approve => :approve, :approve_multiple => :approve, :reject => :reject, :template => :create, :delete_multiple => :delete}

  def new
    new_template = (params[:type] == "template")
    worklog_user_id = params[:user_id].presence || current_user.id
    template = WorkLog.templates.find_by_user_id(worklog_user_id)

    @work_log  = WorkLog.new do |w|
      if (template)
        w.pay_period = WorkLog.next_valid_pay_period
        w.hours = template.clone_hours
      elsif (new_template)
        @title = 'New Template'
        w.template = true
        w.hours.push(Hour.new({:hourtype => "R",:work_log_id => w.id}))
      else
        w.pay_period = WorkLog.next_valid_pay_period
        w.hours.push(Hour.new({:hourtype => "R"}))
      end
      w.user_id = params[:user_id].presence || current_user.id
      w.submit_to = params[:submit_to].presence || (current_user.supervisor.id unless current_user.supervisor.nil?) || User.with_role(:admin).first.id
    end


  end

  def template
    template = WorkLog.templates.find_by_user_id(current_user.id)
    if (template)
      @work_log = template
      render 'edit'
    else
      redirect_to :action => 'new', :type => 'template'
    end
  end

  def create
    @work_log = WorkLog.new(params[:work_log])
    if @work_log.save
      if (@work_log.template?)
        redirect_to work_logs_path, :flash => { :success => "Template Created." }
      else
        UserMailer.approval_email(current_user, User.find(@work_log.submit_to), @work_log, new = true).deliver
        redirect_to work_logs_path, :flash => { :success => "Work Log Submitted Successfully!" }
      end
    else
      flash[:error] = @work_log.errors.full_messages.to_sentence
      @title = "Create Work Log"
      render 'new'
    end
  end
  def update
    if (params[:id] == "process")
        @work_logs = WorkLog.where(:pay_period => params[:pay_period])
        authorize_action_for(@work_logs)
        un = params[:mark].present? ? "" : "un"
        if @work_logs.update_all(:processed => params[:mark].present? ? (DateTime.now) : nil)
          redirect_to work_logs_path, :flash => { :success => "Marked pay period #{params[:payperiod]} as #{un}processed." }
        else
          redirect_to work_logs_path, :flash => { :error => "Failed to mark pay period #{params[:payperiod]} as #{un}processed." }
        end
    else
        @work_log = WorkLog.find(params[:id])

        if (current_user.administrator? || !@work_log.approved? && (current_user.id == @work_log.approved_by_id || current_user.id == @work_log.user_id) )

          if @work_log.update_attributes(params[:work_log])
            if (@work_log.template?)
              redirect_to work_logs_path, :flash => { :success => "Template Updated." }
            else
              UserMailer.approval_email(current_user, User.find(@work_log.submit_to), @work_log).deliver
              redirect_to @work_log, :flash => { :success => "WorkLog re-submitted." }
            end
          else
            flash[:error] = @work_log.errors.full_messages.to_sentence
            @title = "Edit Work Log"
            render 'edit'
          end
        else
            flash[:error] = "Cannot edit approved work log"
            @title = "Show Work Log"
            render 'show'
        end

    end
  end

  def edit
    @work_log = WorkLog.find(params[:id])
    if (@work_log.approved?)
      redirect_to @work_log, :flash => {:error => "Work Log is read only once approved."}
    end
  end

  def approve_multiple
    @work_logs = WorkLog.find(params[:work_log_id])

    @work_logs.each do |w|
      authorize_action_for(w)
      if current_user.id === User.find(w.user_id).supervisor_id || administrator?
          w.approved = true
          w.approved_at = DateTime.now()
          w.approved_by_id = current_user.id
          w.save
      end
    end

    respond_to do |format|
      format.html { redirect_to work_logs_url, notice: "Successfully approved" }
      format.json { head :no_content }
    end
  end

  def approve
    @work_log = WorkLog.find(params[:id])
    authorize_action_for(@work_log)
    if current_user.id === User.find(@work_log.user_id).supervisor_id || administrator?
      @work_log.approved = true
      @work_log.approved_at = DateTime.now()
      @work_log.approved_by_id = current_user.id
      if @work_log.save
          redirect_to work_logs_path, :flash => { :success => "Work Log Approved." }
        else
          redirect_to @work_log, :flash => { :error => "Work Log Approval Failed." }
      end
    else
      redirect_to @work_log, :flash => { :error => "Work Log Approval Failed." }
    end

  end


  def reject
    @work_log = WorkLog.find(params[:id])
    authorize_action_for(@work_log)
    @work_log.reject

    if @work_log.save
        UserMailer.reject_email(current_user, User.find(@work_log.submit_to), @work_log).deliver
        redirect_to work_logs_path, :flash => { :success => "Work Log Rejected." }
    else
        redirect_to @work_log, :flash => { :error => "Work Log Rejection Failed." }
    end

  end

  def destroy
    @work_log = WorkLog.find(params[:id])
    authorize_action_for(@work_log)
    if (!@work_log.approved?)
      if @work_log.destroy
        redirect_to work_logs_path, :flash => { :success => "Work Log Deleted." }
      else
        redirect_to @work_log, :flash => { :error => "Work Log Delete Failed." }
      end
    else
        redirect_to @work_log, :flash => { :error => "Cannot delete a Work Log after it has been approved." }
    end

  end
  def delete_multiple
    @work_logs = WorkLog.find(params[:work_log_id])

    @work_logs.each do |w|
      authorize_action_for(w)
      w.destroy
    end

    respond_to do |format|
      format.html { redirect_to work_logs_url, notice: "Successfully deleted" }
      format.json { head :no_content }
    end
  end

  def index
    @work_logs = WorkLog.joins(:user).allowed_worklogs(@current_user).filter(filtering_params(params)).order('pay_period DESC, last_name').paginate(:per_page => 20, :page => params[:page])
    respond_to do |format|
      format.html  # index.html.erb
      format.json  { render :json => @work_logs }
    end
  end

  def show
    @work_log = WorkLog.find(params[:id])
    authorize_action_for(@work_log)

  end

private

# A list of the param names that can be used for filtering
    def filtering_params(params)
      params.slice(:user_id, :templates, :approved, :pay_period).merge(:templates => (params[:templates] || "false"))
    end

    end
