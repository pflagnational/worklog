class LeaveAccrualRulesController < ApplicationController
  before_filter :require_admin

  def index
    @leave_accrual_rules = LeaveAccrualRule.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @leave_accrual_rules }
    end
  end

  # GET /leave_accrual_rules/1
  # GET /leave_accrual_rules/1.json
  def show
    @leave_accrual_rule = LeaveAccrualRule.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @leave_accrual_rule }
    end
  end

  # GET /leave_accrual_rules/new
  # GET /leave_accrual_rules/new.json
  def new
    @leave_accrual_rule = LeaveAccrualRule.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @leave_accrual_rule }
    end
  end

  # GET /leave_accrual_rules/1/edit
  def edit
    @leave_accrual_rule = LeaveAccrualRule.find(params[:id])
  end

  # POST /leave_accrual_rules
  # POST /leave_accrual_rules.json
  def create
    @leave_accrual_rule = LeaveAccrualRule.new(params[:leave_accrual_rule])

    respond_to do |format|
      if @leave_accrual_rule.save
        format.html { redirect_to @leave_accrual_rule, notice: 'Leave accrual rule was successfully created.' }
        format.json { render json: @leave_accrual_rule, status: :created, location: @leave_accrual_rule }
      else
        format.html { render action: "new" }
        format.json { render json: @leave_accrual_rule.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /leave_accrual_rules/1
  # PUT /leave_accrual_rules/1.json
  def update
    @leave_accrual_rule = LeaveAccrualRule.find(params[:id])

    respond_to do |format|
      if @leave_accrual_rule.update_attributes(params[:leave_accrual_rule])
        format.html { redirect_to @leave_accrual_rule, notice: 'Leave accrual rule was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @leave_accrual_rule.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /leave_accrual_rules/1
  # DELETE /leave_accrual_rules/1.json
  def destroy
    @leave_accrual_rule = LeaveAccrualRule.find(params[:id])
    @leave_accrual_rule.destroy

    respond_to do |format|
      format.html { redirect_to leave_accrual_rules_url }
      format.json { head :no_content }
    end
  end
end
