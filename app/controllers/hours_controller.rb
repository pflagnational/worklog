class HoursController < ApplicationController
  before_filter :require_admin, :except => [:index]
  before_filter :check_key, :only => [:index]

  def index
    respond_to do |format|
      @filter = params.slice(:pay_period,:paid,:user_id,:template)
      @filter[:template] = params[:template] || false
      @hours = Hour.filter(@filter).order('id DESC')
      format.html do
        @hours = @hours.paginate(:per_page => 20, :page => params[:page])
      end
      format.csv do
          @hours
      end
      format.json do
          render :json => @hours.to_json(:only => [], :methods => [:pay_period,:full_name, :adp_id,:hourtype, :dept, :activity, :program, :time, :notes])
      end
    end
  end

  def new
    @hour = Hour.new
  end

  def create
    @hour = Hour.new(params[:hour])
    if @hour.save
      redirect_to @hour, :flash => { :success => "Hour was successfully created!" }
    else
      @title = "Create hour"
      render 'new'
    end
  end

  def destroy
    @hour = Hour.find(params[:id])
    @hour.destroy

    respond_to do |format|
      format.html { redirect_to hours_url }
      format.json { head :no_content }
    end
  end

  def update
    @hour = Hour.find(params[:id])
    respond_to do |format|
      if @hour.update_attributes(params[:hour])
        format.html { redirect_to @hour, notice: 'Hour was successfully updated.' }
        format.json { head :no_content }
      else
        flash[:error] = @hour.errors.full_messages.to_sentence
        format.html { render action: "edit" }
        format.json { render json: @hour.errors, status: :unprocessable_entity }
      end
    end  end

  def edit
    @hour = Hour.find(params[:id])
  end

  def show
    @hour = Hour.find(params[:id])
  end

  private
  def check_key
    code = Code.where(:category => 'Configuration',:code => "Hours Key").first
    unless !code.nil? && params[:key] == code.description
      require_admin
    end
  end

end
