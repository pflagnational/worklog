var debug = true;

function stripLabels() {
	$(".hours-fields:eq(0) label").show();
}

function submit_multiple_delete_check() {
	var f = $(".submit_delete").closest("form");
	if ($("input[type=checkbox][name$='_id[]']:checked", f).length === 0) {
		alert("Nothing selected to delete!");
		return false;
	} else {
		return confirm("Are you sure?");
	}
}

function time(func) {
    var a = performance.now();
    func();
    var b = performance.now();
    console.log('It took ' + (b - a) + ' ms.');
}

function getTimeData(cols) {
    var cols = cols || $("td.hours-fields:not(:hidden), td.accrual-fields:not(:hidden), td#totalColumn:not(:hidden)");
    var totalColumn = $("#totalColumn");
    var totalIndex = 0;
    var time_data = [];
    cols.each(function(j, v) {
        time_data[j] = {};
        time_data[j]["hourtype"] = $("select[id*=hourtype]",v).val() || "total";
        total = 0;
        $("input.day",v).each(function(k,d) {
            var val = $(d).val()*1;
            time_data[j][k] = (time_data[j][k] || 0) + val;
            total = total + val;
       })
        time_data[j]["total"] = total;
    });
    return time_data;
}

function getHoursSummary(time_data) {
    var hours_summary = {}
    var paid_hours = Object.keys(HOURTYPES.PAID_HOURS);
    var leave_hours = Object.keys(HOURTYPES.LEAVE_HOURS);
    var earned_hours = Object.keys(HOURTYPES.EARNED_HOURS);
    
    for (i in time_data) {
        var col = time_data[i];
		var hourtype = col.hourtype;
        //add to hourtype
        hours_summary[col.hourtype] = Math.round(((hours_summary[col.hourtype] || 0) + col.total)*100)/100;
    }
    hours_summary["total_paid_hours"] = 0;
    hours_summary["total_leave_hours"] = 0;
    hours_summary["total_earned_hours"] = 0;

    for (i in paid_hours) {
        var type = paid_hours[i]
        if (hours_summary[type] == undefined) continue;
        hours_summary["total_paid_hours"] += Math.round((hours_summary[type])*100)/100;
    }
    for (i in leave_hours) {
        var type = leave_hours[i]
        if (hours_summary[type] == undefined) continue;
        hours_summary["total_leave_hours"] += Math.round((hours_summary[type])*100)/100;
    }
    for (i in earned_hours) {
        var type = leave_hours[i]
        if (hours_summary[type] == undefined) continue;
        hours_summary["total_leave_hours"] += Math.round((hours_summary[type])*100)/100;
    }
   return hours_summary;
}


function update_work_log_summary(hours_summary) {
    /*
        Update work log summary block above leave balances
    */
    var hours_summary = hours_summary || getHoursSummary(getTimeData());
    var accrued_leave = 0;
	$("dd.accrued span").each(function() { accrued_leave += parseFloat($(this).html()) || 0 });
	var total = parseFloat($("#totalColumn input.sub-total").val()) || 0;
	$("#total_paid_hours").html(hours_summary.total_paid_hours);
	$("#total_earned_hours").html(hours_summary.total_earned_hours);
	$("#total_accrued_hours").html(accrued_leave);
	$("#total_leave_hours").html(hours_summary.total_leave_hours);
	$("#total_reported_hours").html($("input.sub-total",totalColumn).val());
    if (hours_summary.total_paid_hours == 80) {
      $("p.text-warning:contains('Exception')").hide();   }
    else {
      $("p.text-warning:contains('Exception')").show();   
    }

}


function setWorkLogTabOrder() {
	var cols = $("td.hours-fields").length;
	var rows = $("input,select","td.hours-fields:nth(0)").length;
	var i=1;

	for(var row=0;row<rows;row++) {
		for(var col=0;col<cols;col++) {
			var div = $("div:nth("+row+")","td.hours-fields:nth("+col+")");
			$("input, select",div).attr("tabindex",i++);
		}

	}

	$("input.btn.btn-primary[type=submit]").attr("tabindex",i++);
	$(".form-actions a.btn:contains(Cancel)").attr("tabindex",i++);
	$("input.day:nth(0)","td.hours-fields:nth(0)").focus();

}

function setup_add_remove_buttons() {
    var tc = $("a.add_fields").attr("data-association-insertion-template")
    if (tc == undefined) return;
    tc.replace(/new_hours/g,"total"); 
//    var tc = $(".work-log-hours-table td").eq(1).html();
    $("#totalColumn").replaceWith($(tc).removeClass().attr("id","totalColumn"));//$(".work-log-hours-table td").eq(1).html()); //use: $("a.add_fields").attr("data-association-insertion-template")?
	totalColumn = $("#totalColumn");
	$("div:eq(0)",totalColumn).height(35).css("padding-bottom",0);
	$("div:last",totalColumn).height(35).css("padding",0);
	$("select",totalColumn).replaceWith($("<input type='text'/>").addClass("text_field, row-total"));
	$("input",totalColumn).width(30).attr("disabled","true");
	$.each([0,1,2,3,19],function(i,v) {
		$("input",totalColumn).eq(v).val("");
	});

	$("#totalColumn .remove_fields").replaceWith($(".add_fields").eq(0).clone());
	$("#totalColumn div:first,#totalColumn div:last").css("padding-top",15);
	$("#totalColumn").appendTo($("#totalColumn").parent());
	resize_hours();
	$(".add_fields").not(".add_fields:first").show();
	$(".work-log-hours-table").bind('cocoon:after-insert', setWorkLogTabOrder);

}

function on_worklog_submit() {
	update_leave_totals();
	
	var total_paid_hours = parseFloat($("#total_paid_hours").html());
	if (total_paid_hours != 80) {
		var c = confirm("Total Paid Hours != 80. Are you sure you want to submit?");
    	return c; //you can just return c because it will be true or false
	}
    else {
    	 return true;
    }
	$(".row-total").removeAttr("name").removeClass(".sub-total");	
	$(".sub-total").removeAttr("disabled");
	update_leave_totals();
}

function change_dates() {
	if (new Date($("#work_log_pay_period").val()) == "Invalid Date") return;
	$("label[for*=day]").each(function (index) {
		var t = new Date($("#work_log_pay_period").val());
		t.setDate(t.getDate()+index-12);
		t=t.toDateString().split(" ")
		$(this).html(t[0] + ", " + t[1] + " " + t[2])

	});

}

function resize_hours() {
	var max_width = $("div.container:first").width()-$("td#totalColumn").width();
	$(".control-label").width(100);
	var field_width = Math.min(150, ((max_width-225)/($(".hours-fields").length)-17));
	field_width = Math.max(35,field_width);
	$("td.hours-fields input, td.accrual-fields input").width(field_width);
	$("td.hours-fields select, td.accrual-fields select").width(field_width+12);

}

function worklog_changed(e) {
    /*
        fired when worklog changed 
    */
    var time_data = getTimeData();
    updateHoursTotal(time_data)
    update_leave_totals(time_data);
    update_work_log_summary();
}

function updateHoursTotal(time_data) {
    /*
        Updates hours totals on work log table (both rows and columns)
    */
    var cols = $("td.hours-fields:not(:hidden), td.accrual-fields:not(:hidden), td#totalColumn:not(:hidden)");
    var time_data = time_data||getTimeData(cols);
    var total_column_index = -1;
    var total_column = {}
    var total = 0;
    for(i in time_data) {
        var col = time_data[i] //iterate columns
        var hourtype = time_data[i].hourtype;
        for (j in col) { //iterate rows
            var cell = col[j];
            var is_day = j.match(/[0-9]+/); //true if a day value
            var is_total = (j == "total");
            var include_in_total = hourtype != "total" && !hourtype.match(/[A-Z][A]/);
            if (include_in_total && (is_day || is_total)) {
                total_column[j] = (total_column[j] || 0) + cell;
            }
        }
    }

    /* update column totals */
    $(".sub-total",cols).each(function(k,v) {
        $(v).val(Math.round(time_data[k]["total"]*100)/100);
    });
    
    /*update total column days*/
    $("input.day",totalColumn).each(function(k,v) {
        $(v).val(Math.round(total_column[k]*100)/100);
    })
    
    /*update total of all cells*/
    $("input.sub-total",totalColumn).val(Math.round(total_column["total"]*100)/100);
    
}

function html_to_float(html) {
    return parseFloat(html.html()) || 0
}

function update_leave_totals(time_data) {
    /*
        Updates Leave Balances & Activity Detail
        Calls update work log summary afterwards
    */
    var cols = cols || $("td.hours-fields:not(:hidden), td.accrual-fields:not(:hidden), td#totalColumn:not(:hidden)")
    var time_data = time_data || getTimeData(cols);
    
	$("dd.used.leave_detail span").html(0)
	$("dd.earned.leave_detail span").html(0)

	$(".leave_net").each( function() {
		var hourtype = $(this).attr("data-hourtype").charAt(0)
		var begbal_span = $("."+hourtype+"_leave_detail.beginning span");
		var accrued_span = $("."+hourtype+"_leave_detail.accrued span");
		var used_span = $("."+hourtype+"_leave_detail.used span");
		var earned_span = $("."+hourtype+"_leave_detail.earned span");
        
        var begbal = html_to_float(begbal_span);
        var accrued = html_to_float(accrued_span);
        var used = total_by_hourtype(hourtype,time_data);
        var earned = total_by_hourtype(hourtype+"E",time_data);
        /* begining + accrued - used + earned */
		var total = round2(begbal + accrued - used + earned);
        if (debug) console.log("update_leave_totals:" + hourtype + ":" + begbal + ":" +  accrued +":"+ used +":"+ earned + ":" + total);
        /* update earned */
        earned_span.html(earned);
        /* update used */
        used_span.html(used);
        /* update net total*/
		$("span",$(this)).html(total)

	})
}

function round2(num) {
    // round to two decimal places
    return Math.round(num*100)/100;   
}

function time_data_by_hourtype(hourtype,time_data) {
    if (time_data == undefined)
        time_data = getTimeData();
    if (typeof hourtype == "string")
        return time_data.filter(function(e) { return e.hourtype == hourtype});
    else if (typeof hourtype == "object")
        return time_data.filter(function(e) { return hourtype.indexOf(e.hourtype) > -1});
}

function total_by_hourtype(hourtype,time_data) {
    if (time_data == undefined)
    time_data = getTimeData();
    var td = time_data_by_hourtype(hourtype)
    var total = 0;
    for (i in td) {
        total = total + td[i].total;
    }
    return total;
}

function disable_program_and_activities_for_leave_hour_types() {

	$("td.hours-fields").each(function() {
	var code = $("select[name*=hourtype] option:selected",$(this)).val();
	if (code == 'R' || code == 'CE') {
		$("select[name*=program]",$(this)).removeAttr("disabled")
		$("select[name*=activity]",$(this)).removeAttr("disabled")

	} else { /*disable program and activity*/
		$("select[name*=program]",$(this)).attr("disabled","true")
		$("select[name*=activity]",$(this)).attr("disabled","true")
	}

});
}

function initialize_leave_balance_detail_expand_collapse() {
	/*leave detail expand collapse*/
	$("dd[class*=leave_detail]").hide()
	$("dd.leave_net a").click(function(e) { 
		e.preventDefault();
		c = $(this).attr("data"); 

		if ($(this).hasClass("icon-plus")) {
			$("."+c).show();
			$(this).removeClass("btn-primary").removeClass("icon-plus").addClass("icon-minus").addClass("btn-danger");
			}
			else {
			$("."+c).hide();
			$(this).removeClass("btn-danger").removeClass("icon-minus").addClass("icon-plus").addClass("btn-primary");
		
			}

	 })	
	/* end leave detail expand collapse*/


}

leave_accrual_field_template = '<fieldset class="leaveaccrual" data-type=""><input id="work_log_hours_attributes_0_id" name="work_log[hours_attributes][0][id]" type="hidden" value=""><input id="work_log_hours_attributes_0_hourtype" name="work_log[hours_attributes][0][hourtype]" type="hidden" value=""><input id="work_log_hours_attributes_0_distributed_time" name="work_log[hours_attributes][0][distributed_time]" type="hidden" value=""></fieldset>'

function leave_balances_json_update() {
 	var user_id = $("[id*=user_id]").val();
    var form = $("form[id*=WorkLog]");
    var action = form.attr("id").split("_")[0];
    if (action == "new")
      $("fieldset.leaveaccrual").remove()
    
	$.getJSON('/leave_balances.json', {user_id: user_id, pay_period: $('#work_log_pay_period').val()}, function(data) {

        build_leave_balances_activity_detail(data)
		worklog_initialization_after_leave_balance_load();
	});
    initialize_show_view();

}

function build_leave_balances_activity_detail(data) { //populate leave_balance_detail_from_json
	var leave_balances_html = [];
    $.each(data, function(key, val) {
        var uploaded = Date.parse(val.uploaded);
        var pay_period = Date.parse($("#work_log_pay_period").val());
        if (pay_period < uploaded) { 
            leave_balances_html.push("<p>Historical Values for " + val.category_name + " unavailable. Check <a href=\"/leave_balances?user_id="+ val.user_id +"\">My Leave Balances</a> for current balances.</p>"); 
            return;
        }
        var category = val.category;
        if(debug) {
            console.log("processing: " + category);
            console.log("accrued: " + val.accrued_this_pay + " code: " + category);
        }


        leave_balances_html.push('<dt><strong>' + val.category_name + ':</strong></dt>');
        leave_balances_html.push('<dd class="' + category + '_leave_detail leave_detail beginning" data-hourtype="'+ category + '" style="display:none">Beginning: <span>' + ((parseFloat(val.balance) || 0)) + '</span></dd>');
        leave_balances_html.push('<dd class="' + category + '_leave_detail leave_detail accrued" data-hourtype="'+ category + '" style="display:none">Accrued: <span>' + val.accrued_this_pay +'</span></dd>');
        leave_balances_html.push('<dd class="' + category + '_leave_detail leave_detail earned" data-hourtype="'+ category + '" style="display:none">Earned: <span>' + val.earned_this_pay + '</span></dd>');
        leave_balances_html.push('<dd class="' + category + '_leave_detail leave_detail used" data-hourtype="'+ category + '" style="display:none">Used: <span>' + val.used + '</span></dd>');
        leave_balances_html.push('<dd class="' + category + '_leave leave_net" data-hourtype="'+category+'"><strong><span>' + val.net_leave_balance + '</span> <a href="#" class="btn btn-primary btn-small icon-plus" data="' + category  + '_leave_detail"></a></strong></dd>');

    });   

    $(".leave_balances_and_activity_json").html(leave_balances_html.join("\n"));

}

function responsive_table() {
	$("#no-more-tables th").each(function(index,value) {
		$("#no-more-tables td:nth-child("+(index+1)+")").attr("data-title",$(this).text());
	});
}

function worklog_initialization_after_leave_balance_load () {
		responsive_table();
		$(".day").change(worklog_changed);
		$('.work-log-hours-table').bind('cocoon:after-insert', function(e, inserted_item) {
			$("input.day",inserted_item).change(worklog_changed);
			$("select[name$=hourtype\\]]",inserted_item).change(disable_program_and_activities_for_leave_hour_types);
			setup_add_remove_buttons();
			disable_program_and_activities_for_leave_hour_types();
			worklog_changed();

		});
		$('.work-log-hours-table').bind('cocoon:after-remove', function(e, item) {
			$("[name*=destroy]", item).val(1);
			setup_add_remove_buttons();
			worklog_changed();
		});
		$("#work_log_pay_period").change(change_dates);
		change_dates();
		setup_add_remove_buttons();
		worklog_changed();
		resize_hours();
		$("#work_log_user_id").change(leave_balances_json_update);
		$(".add_fields:first").hide()

		/*hours search on change of dropdown*/
		$("#hours_search select#search").change(function() { $("#hours_search").submit(); });

		initialize_leave_balance_detail_expand_collapse();

		$("td.hours-fields select[name*=hourtype]").change(disable_program_and_activities_for_leave_hour_types);
		if (!($("#show_WorkLog").length>0)) disable_program_and_activities_for_leave_hour_types();
		setWorkLogTabOrder();
        initialize_show_view();

}

function initialize_show_view() {
  $("#show_WorkLog input, #show_WorkLog select").attr("disabled",true);
  $("#show_WorkLog a.remove_fields").hide();
  $("#show_WorkLog a.add_fields").hide();
/*  resize_hours();
  initialize_leave_balance_detail_expand_collapse();*/
}

$().ready(function() {
	$(".dropdown-toggle").dropdown()
	$(".submit_delete").click(submit_multiple_delete_check);

	if($(".day").length > 0) { //worklog form JS
		leave_balances_json_update();
	}

	if($("dd[class$=_leave_detail]"))
		initialize_leave_balance_detail_expand_collapse();

	responsive_table();

	$("form[action*=worklogs][id*=work_log]").submit(on_worklog_submit);
    $('tbody.rowlink').rowlink()

});
