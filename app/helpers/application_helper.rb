module ApplicationHelper

  # Returns the full title on a per-page basis.       # Documentation comment
  def full_title(page_title)                          # Method definition
    base_title = "Work Logs"  # Variable assignment
    if page_title.empty?                              # Boolean test
      base_title                                      # Implicit return
    else
      "#{base_title} | #{page_title}"                 # String interpolation
    end
  end

  def flash_class(level)
    case level
    when :notice then "info"
    when :success then "info"
    when :error then "error"
    when :alert then "warning"
    end
  end

  def logo
    image_tag("logo.png", :width => "50px", :height => "50px", :alt => "PFLAG Logo", :class => "round")
  end

end