# == Schema Information
#
# Table name: users
#
#  id                :integer          not null, primary key
#  provider          :string(255)
#  uid               :string(255)
#  email             :string(255)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  adp_id            :integer
#  department_id     :integer
#  users             :string(255)
#  first_name        :string(255)
#  last_name         :string(255)
#  supervisor_id     :integer
#  hire_date         :date
#  accrual_type      :string(255)
#  alias_for_user_id :integer          default(-1)
#  password_digest   :string(255)
#

class User < ActiveRecord::Base
  rolify
  include Authority::UserAbilities 
  attr_accessible :provider, :uid, :name, :email, :adp_id, :department_id, :first_name, :last_name, :supervisor_id, :accrual_type, :hire_date, :alias_for_user_id, :role_ids
  has_many :leave_balances
  has_many :work_logs
  has_many :hours, :through => :work_logs
  default_scope order('last_name ASC')
  scope :administrators, Role.find_by_name("admin").users if Role.table_exists? and Role.find_by_name("admin") != nil
  scope :supervisors, Role.find_by_name("supervisor").users if Role.table_exists? and Role.find_by_name("supervisor") != nil

  scope :not_aliases, where{alias_for_user_id.eq(-1)}
  scope :aliases, where('alias_for_user_id > -1')
  validates_uniqueness_of :uid, :scope => :provider
  validates_uniqueness_of :adp_id, :unless => lambda { :alias_for_user_id || 0 > 0 }


  def self.from_omniauth(auth)
    user =  User.where(auth.slice("provider", "uid")).first || create_from_omniauth(auth)
    until user.not_alias? do
      user = User.find(user.alias_for_user_id)
    end
    user
  end

  def years_of_service(date = Date.today)
      date1 = self.hire_date
      date2 = date
      month = (date2.year * 12 + date2.month) - (date1.year * 12 + date1.month)
      a = month.divmod(12)
      a[0] + a[1]/12
  end

  def not_alias?
    not self.is_alias?
  end

  def is_alias?
    self.alias_for_user_id != nil && (self.alias_for_user_id > 0)    
  end

  def per_pay_leave_accrual_rules(paydate = Date.today)
    years = self.years_of_service(paydate)
    LeaveAccrualRule.where(accrual_type: self.accrual_type, accrual_frequency: "Per Pay").select{|h| 
         h.check(years)
        }
  end

  def self.create_from_omniauth(auth)
    email = auth["info"]["email"]
    users_with_email = User.where(email: email)
    domain = /@(.+$)/.match(email)[1]
    if (domain.casecmp("pflag.org") != 0)
      raise UserDomainError, "#{domain} is an invalid email address domain."
    else
      if (users_with_email.any?)
        user = users_with_email.first
        user.provider = auth["provider"]
        user.uid = auth["uid"]
        user.add_role :employee
        user.save
      else
        create! do |user|
          user.provider = auth["provider"]
          user.uid = auth["uid"]
          #user.name = auth["info"]["nickname"]
          user.email = auth["info"]["email"]
          user.first_name = auth["info"]["first_name"]
          user.last_name = auth["info"]["last_name"]
          user.add_role :admin if (User.all.count == 0)
        end
      end
    end
    user
  end

  def full_name
    "#{self.first_name} #{self.last_name}"
  end

  def direct_reports
    User.find_all_by_supervisor_id(self.id)
  end


  def supervisor
      User.find(self.supervisor_id) if User.exists? (self.supervisor_id) 
  end

  def supervisor?
    self.has_role? :supervisor
  end
  def administrator?
    self.has_role? :admin
  end

  def direct_reports_leave_balances
    
    if self.administrator?
        LeaveBalance.active_employees
    elsif self.supervisor?
        user_ids = self.direct_reports.collect(&:id).push(self.id)
        LeaveBalance.active_employees.where(:user_id => user_ids)
    else
        LeaveBalance.user_id(self.id)
    end
        
  end

  private
    def compare(a,b,op)
       case op 
        when "<"
            a < b
        when "<="
            a <= b
        when ">"
            a > b
        when ">="
            a >= b
        else
           true
       end
        
    end

    def default_hire_date
      if @hire_date.nil?
        @hire_date = Date.today()
      end

    end

end


class UserDomainError < StandardError
end
