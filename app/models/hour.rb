# == Schema Information
#
# Table name: hours
#
#  id          :integer          not null, primary key
#  program     :string(255)
#  activity    :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  hourtype    :string(255)
#  notes       :string(255)
#  work_log_id :integer
#  saturday_1  :decimal(, )
#  sunday_1    :decimal(, )
#  monday_1    :decimal(, )
#  tuesday_1   :decimal(, )
#  wednesday_1 :decimal(, )
#  thursday_1  :decimal(, )
#  friday_1    :decimal(, )
#  saturday_2  :decimal(, )
#  sunday_2    :decimal(, )
#  monday_2    :decimal(, )
#  tuesday_2   :decimal(, )
#  wednesday_2 :decimal(, )
#  thursday_2  :decimal(, )
#  friday_2    :decimal(, )
#  time        :decimal(, )
#

class Hour < ActiveRecord::Base
  belongs_to :work_log, dependent: :destroy
  has_one :user, through: :work_log
  attr_accessible :activity, :program, :saturday_1, :sunday_1, :monday_1, :tuesday_1, :wednesday_1, :thursday_1, :friday_1, :saturday_2, :sunday_2, :monday_2, :tuesday_2, :wednesday_2, :thursday_2, :friday_2, :hourtype, :work_log_id, :notes, :time, :distributed_time
  include Filterable
  scope :user_id, -> (user_id) { joins(:user).where("users.id = ?",user_id) }
  scope :pay_period, -> (pay_period) { joins(:work_log).where("work_logs.pay_period" => pay_period) }
  scope :template, -> (template) { joins(:work_log).where("work_logs.template" => template)}
  validates :time,  :numericality => { :greater_than => 0 }
  before_validation :total_time
  validates :program, :activity, :presence => true, :if => :coded_hourtype?
  #scope based on paid hours in Code class / configuration tables
  scope :accrual, -> { where("hourtype" => Code.paid_hour_types.inject([]){|c,h| c.push("#{h.code}")}) }
  scope :paid, -> { where("hourtype" => Code.paid_hour_types.inject([]){|c,h| c.push("#{h.code}")}) }
  #scope based on paid leave scope in Code class / configuration tables
  scope :paid_leave, -> { where("hourtype" => Code.leave_types.inject([]){|c,h| c.push("#{h.code}")}) }
  scope :type, -> (type) { where hourtype: type }
  DAY_FIELDS = [:saturday_1, :sunday_1, :monday_1, :tuesday_1, :wednesday_1, :thursday_1, :friday_1, :saturday_2, :sunday_2, :monday_2, :tuesday_2, :wednesday_2, :thursday_2, :friday_2]


  def distribute_time(time)
      if time
        split = (time.to_d*1/14).round(2)
        DAY_FIELDS.each{|k,v| self[k] = split}
        self['friday_2'] = (time.to_d-split*13)
      end
      self.total_time
  end

  def distributed_time=(time)
      self.distribute_time(time)
  end

  def distributed_time
    self.total_time
  end

  def pay_period
    if self.work_log.present?
  	   self.work_log.pay_period
    else
       ""
    end
  end

  def coded_hourtype?
    self.hourtype == 'R'
  end
  def accrual?
    code = Code.hour_types.find_by_code(self.hourtype)
    !code.blank? and code.restriction == "Accrued Leave"
  end

  def full_name
    if self.work_log.present? && self.work_log.user.present?
      self.work_log.user.full_name
    else
      ""
    end
  end

  def hour_type_name
    code = Code.hour_types.find_by_code(self.hourtype)
    code.blank? ? "N/A" : code.description
  end

  def activity_name
    activity = Code.find_by_category_and_code("Activity",self.activity)
    if (activity.blank?)
      ""
    else
      activity.description
    end
  end

  def program_name
    program = Code.find_by_category_and_code("Program",self.program)
    if (program.blank?)
      ""
    else
      program.description
    end
  end


  def adp_id
    if self.work_log.present? && self.work_log.user.present?
  	   self.work_log.user.adp_id
    else
       ""
    end
  end

  def dept
    if self.work_log.present? && self.work_log.user.present?
      self.work_log.user.department_id
    else
      ""
    end
  end

	def self.to_csv(options = {})
	  CSV.generate(options) do |csv|
	    csv << "id,program,activity,hourtype"
	    all.each do |hour|
	      csv << hour.attributes.values_at(*column_names)
	    end
	  end
	end



	def total_time
    self.time = 0
    [saturday_1, sunday_1, monday_1, tuesday_1, wednesday_1, thursday_1, friday_1, saturday_2, sunday_2, monday_2, tuesday_2, wednesday_2, thursday_2, friday_2].each do |t|
      t = t.to_s.to_d if (!t.nil? && t.class.name == 'FixNum')
      self.time += t.to_d unless t.nil?
    end
    self.time = self.time.round(2)
  end


end
