# == Schema Information
#
# Table name: leave_balances
#
#  id         :integer          not null, primary key
#  category   :string(255)
#  balance    :decimal(, )
#  uploaded   :date
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#

class LeaveBalance < ActiveRecord::Base
  attr_accessible :balance, :category, :uploaded, :user_id, :accrued
  belongs_to :user
  resourcify
  include Authority::Abilities
  scope :active_employees, joins(:user).order("users.last_name asc, users.first_name asc, category asc").where(:user_id => Role.find_by_name("employee").users.map{|h| h.id}) if Role.table_exists? and Role.find_by_name("employee") != nil

    
  validates :balance, :category, :uploaded, :user_id, :presence => true
  validate :uploaded_date_not_before_hire_date

  include Filterable
  scope :user_id, -> (param = nil) { 
      u = param.to_i
      if u > 0
         where("user_id = ?", u)
      elsif u < 0
         where("user_id != ?",-u)
      else 
         where("")
      end
    }
  def uploaded_date_not_before_hire_date
    if self.uploaded < User.find(self.user_id).hire_date
      errors.add(:uploaded, "Date can't be before hire date")
    end
  end


  def accrued(pay_period = WorkLog.next_valid_pay_period)
  	accrued = 0
    @hours = Hour.joins(:work_log).where('work_logs.user_id = ? AND work_logs.pay_period < ? AND hourtype = ?',self.user_id, pay_period.to_date, "#{self.category}A")
    @hours.each do |h|
       accrued += h.time 
    end
#    WorkLog.valid_pay_periods.each do |p|
#		next if p[1].to_date < self.uploaded.to_date || p[1].to_date.future? || p[1].to_date > pay_period
#		accrued += apply_accrual_rule(p[1].to_date)
#  	end
  	accrued
  end

  def accrued_balance
    @accrued_balance = self.accrued + self.balance
  end

  def earned_this_pay(pay_period = WorkLog.next_valid_pay_period)
    earned = 0

    @hours = Hour.joins(:work_log).where('work_logs.user_id = ? AND work_logs.pay_period = ? AND hourtype = ?',self.user_id, pay_period.to_date, "#{self.category}E")
    @hours.each do |h|
       earned += h.time 
    end
    earned    
  end

  def accrued_this_pay(pay_period = WorkLog.next_valid_pay_period)
    @hours = Hour.joins(:work_log).where('work_logs.user_id = ? AND work_logs.pay_period = ? AND hourtype = ?',self.user_id, pay_period.to_date, "#{self.category}A")
    accrued = 0
    @hours.each do |h|
       accrued += h.time 
    end
    if @hours.empty?
      accrued = apply_accrual_rule(pay_period.to_date)
    end
    accrued
  end

  def category_name
    category = Code.find_by_category_and_code("Leave", self.category)
    if category.nil?
      self.category
    else
      category.description
    end

  end

  def used(pay_period = WorkLog.next_valid_pay_period)
  	used = 0
    @logs = WorkLog.find(:all, :include=> :hours, :conditions => ['user_id = ? AND pay_period > ? and hours.hourtype = ? and pay_period <= ?', self.user_id, self.uploaded, self.category, pay_period ])
      @logs.each do |w|
        w.hours.each do |h|
         used += h.time
      end
    end
    used
  end

  def net_leave_balance(pay_period = WorkLog.next_valid_pay_period)
    balance + accrued(pay_period) + earned(pay_period) - used(pay_period)
  end

  def years_of_service(pay_period = WorkLog.next_valid_pay_period)
  	((pay_period - self.user.hire_date)/365).to_i
  end

  def apply_accrual_rule(pay_period=WorkLog.next_valid_pay_period, annual = false)
  	years_of_service = years_of_service(pay_period)
  	rules_set = LeaveAccrualRule.find_all_by_leave_code_and_accrual_type(self.category,self.user.accrual_type)
  	rules_set.delete_if do |r| 
        delete = false
  		case r.condition_type #year of service dependent type
  		when '<'
  			delete = true unless years_of_service < r.condition_unit
		  when '<='
  			delete = true unless years_of_service <= r.condition_unit
  		when '>'
  			delete = true unless years_of_service > r.condition_unit
  		when '>='
  			delete = true unless years_of_service >= r.condition_unit        
  		else #annual and other
            delete = true if r.accrual_frequency == "Annual" && annual == false
  		end
 	  end

 	  if rules_set[0].nil? 
      0
    else
      rules_set[0].unit
    end
  end

  def earned(pay_period = WorkLog.next_valid_pay_period)
    earned = 0

    @hours = Hour.joins(:work_log).where('work_logs.user_id = ? AND work_logs.pay_period > ? AND hourtype = ? AND work_logs.pay_period <=?',self.user_id, self.uploaded, "#{self.category}E",pay_period)
    @hours.each do |h|
       earned += h.time 
    end
    earned    
  end

  def self.search(search)
    if search
      leave_balances = LeaveBalance.find_all_by_user_id(search)
    else
      scoped
    end
  end
    def as_json(options = { })
    if options[:pay_period].blank?
        pay_period = Date.today
    elsif options[:pay_period].class == Date
        pay_period = options[:pay_period]
    else
        pay_period = Date.parse(options[:pay_period])
    end
      {  balance: self.balance,
        category: self.category,
        pay_period: pay_period,
        created_at: self.created_at,
        id: self.id,
        updated_at: self.updated_at,
        uploaded: self.uploaded,
        user_id: self.user_id,
        category_name: self.category_name,
        net_leave_balance: self.net_leave_balance(pay_period),
        accrued_this_pay: self.accrued_this_pay(pay_period),
        earned_this_pay: self.earned_this_pay(pay_period),
        accrued: self.accrued(pay_period),
        used: self.used }
    end 

  def self.import(file, effective_date)
    errors = Array.new
    valid_codes = Code.leave_types.collect {|x| x.code}
    if effective_date == nil
        effective_date = Date.today()
    end

    CSV.foreach(file.path, headers: true, skip_blanks: true, encoding: "bom|utf-8") do |row|
      user = User.find_by_adp_id(row["adp_id"])
      user_id = user.id unless user.nil?
      adp_id = row["adp_id"]
      uploaded = row["uploaded"]
      category = row["category"]
      balance = row["balance"]
      balance = balance.include?(")") ? balance.gsub(/\(|\)/, "").insert(0, "-").to_d : balance.to_d
      row["balance"] = balance

      if !(valid_codes.include? category)
        errors.push "Invalid Leave Code #{category} for Payroll ID #{adp_id}"
        next
      end

      if user_id.nil?
        errors.push "Payroll ID #{adp_id} not found, record skipped for category #{category}"
        next
      end

      row.push("user_id" => user_id)

      if row['uploaded'].nil? || row['uploaded'] == ""
        row.push("uploaded" => effective_date) 
      end

      row.delete_if do |k,v|
        true unless ['category','balance','user_id','uploaded'].include? k
      end

      leave_balance = LeaveBalance.find_by_user_id_and_category(user_id,row["category"]) || new
      leave_balance.attributes = row.to_hash
      leave_balance.save!
    end   
    errors
  end
end


