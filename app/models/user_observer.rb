class UserObserver < ActiveRecord::Observer
  def after_create(user)
    UserMailer.new_user_email(user).deliver
  end
end