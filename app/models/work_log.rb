# == Schema Information
#
# Table name: work_logs
#
#  id             :integer          not null, primary key
#  user_id        :integer
#  pay_period     :date
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  submit_to      :integer
#  approved       :boolean
#  approved_at    :datetime
#  approved_by_id :integer
#  template       :boolean
#  processed      :datetime
#

class WorkLog < ActiveRecord::Base
  resourcify
  include Authority::Abilities
  include Filterable
  has_many :hours
  belongs_to :user
  attr_accessible :pay_period, :hours_attributes, :user_id, :submit_to, :approved, :approved_at, :approved_by_id, :template
  default_scope order('pay_period DESC')

  #validate :unique_by_pay_period, :unless => :template?
#  validate :only_one_template, :unless => :not_template?
  validates :pay_period, :presence => true, :uniqueness => {:scope => :user_id}, :unless => :template?
  before_validation :add_accrual_if_missing, :only_one_accrual

  accepts_nested_attributes_for :hours,
          :reject_if => :invalid_hour_attributes,
          :allow_destroy => true

#  scope :approved, where(:approved => true, template: [nil, false])
  scope :approved, -> (param = true) {
      case param
      when true, "true"
          where(:approved => true)
      when false, "false"
          where(approved: [nil, false])
      else
          where("")
      end
    }
#  scope :not_approved, where(approved: [nil, false], template: [nil, false])
#  scope :not_templates, where(template: [nil, false])
  scope :templates, -> (param = [nil, false]) {
      case param
        when false, "false"
          where(:template => [nil, false])
        when true, "true"
          where(:template => true)
        else
          where(:template => true)
      end
    }
  scope :user_id, -> (param = nil) {
      u = param.to_i
      if u > 0
         where("user_id = ?", u)
      elsif u < 0
         where("user_id != ?",-u)
      else
         where("")
      end
    }
  scope :allowed_worklogs, -> (user = nil) { (user.has_role? :admin) ? WorkLog.where("") : WorkLog.where('user_id IN (?) OR submit_to = ?',user.direct_reports.push(user), user) }

  scope :pay_period, -> (pay_period) { WorkLog.where(:pay_period => pay_period) }

  def add_accrual_if_missing
      unless self.user.blank?
          self.user.per_pay_leave_accrual_rules.each do |r|
            h = Hour.new({hourtype: r.leave_code+"A"})
            h.distribute_time(r.unit)
            self.hours.push(h) unless self.hours.find_by_hourtype(r.leave_code+"A")
          end
      end
  end

  def only_one_accrual
    Code.accrued_leave_types.each do |c|
        self.hours.type(c.code).each_with_index do |h, index|
            h.destroy if index > 0
        end

    end

  end

  def full_name
    if self.user_id.nil?
      user = nil
    else
      user = User.find_by_id(self.user_id || 1)
    end
    user.nil? ? "" : user.full_name
  end

  def not_template?
    !self.template
  end

  def pay_period_to_s
    self.pay_period.nil? ? WorkLog.next_valid_pay_period.to_date.to_s : self.pay_period.to_date.to_s
  end

  def leave_balances_not_negative
    hours.each do |h|
      lb = LeaveBalance.find_by_user_id_and_category(self.user_id, h.hourtype)
      next if lb.nil?

      if ( h.time > lb.net_leave_balance )
        errors.add(:leave_balance,"#{lb.category_name} can't be negative")
      end
    end
  end


  def hours_summary
    hours.sum(:time, :group => 'hourtype')
  end

  def find
    hours.sum(:time, :group => 'hourtype')
  end

  def hours_without_earned
    h = 0;
    hours_summary.delete_if { |key, value| key.to_s.match(/[a-z,A-Z][A,E]/) }.each do |key, value|
      h += value;
    end
    h
  end

  def hours_earned
    h = 0;
    hours_summary.select { |key, value| key.to_s.match(/[a-z,A-Z]E/) }.each do |key, value|
      h += value;
    end
    h
  end

  def invalid_hour_attributes(attrs)
    blank = (attrs['saturday_1'].blank? &&
    attrs['sunday_1'].blank? &&
    attrs['monday_1'].blank? &&
    attrs['tuesday_1'].blank? &&
    attrs['wednesday_1'].blank? &&
    attrs['thursday_1'].blank? &&
    attrs['friday_1'].blank? &&
    attrs['saturday_2'].blank? &&
    attrs['sunday_2'].blank? &&
    attrs['monday_2'].blank? &&
    attrs['tuesday_2'].blank? &&
    attrs['wednesday_2'].blank? &&
    attrs['thursday_2'].blank? &&
    attrs['friday_2'].blank? &&
    attrs['distributed_time'].blank?)
  end

  def reports
    where('submit_to = ?',current_user, ' OR user_id IN ?',current_user.direct_reports)
  end

  def submit_to_full_name
    if (submit_to.nil?)
      "n/a  "
    else
      User.find(self.submit_to).full_name
    end
  end
  def self.search(user, params = {})
      @worklogs = allowed_worklogs(user)
      @worklogs = @worklogs.approved(params[:approved])
      if params[:search] == "templates"
          @worklogs = @worklogs.templates
      else
          @worklogs = @worklogs.templates?(false)
      end
      @worklogs = @worklogs.where('user_id == ?', user.id) if params[:search] == "mine"
      @worklogs = @worklogs.where('user_id != ?', user.id) if params[:search] == "not_mine"
      @worklogs
  end

  def self.next_valid_pay_period
    pay_period = Date.parse("2011-10-28")
    loop do
      pay_period += 14
      break if pay_period >= Date.today
    end
    @next_valid_pay_period = pay_period
  end

  def self.last_valid_pay_period
    pay_period = Date.parse("2011-10-28")
    loop do
      pay_period += 14
      break if pay_period >= Date.today
    end
    @next_valid_pay_period = pay_period-14
  end

  def self.valid_pay_periods
    start_pay_period = Date.parse("2011-10-28")
    pay_period_array = []
    (0..156).each do |i|
      paydate = start_pay_period+i*14
      pay_period_array.push(
        [ paydate.strftime("%b %d, %Y"), paydate.strftime("%Y-%m-%d") ]
                                )
    end
    @valid_pay_periods = pay_period_array
  end

  def clone_with_associations
    new_work_log = self.dup
    new_work_log.template = false
    new_work_log.pay_period = WorkLog.next_valid_pay_period;
    new_work_log.save
    self.hours.each do |h|
      new_hours = h.dup
      h.save
      new_work_log.hours << new_hours
    end
    new_work_log
  end

  def reject
     self.approved = nil
     self.approved_at = Time.at(1318996912).to_datetime
     self.approved_by_id = nil
  end

  def clone_hours
    new_hours = []
    self.hours.each do |h|
      new_hour = h.dup
      h.save
      new_hours << new_hour
    end
    new_hours
  end

end
