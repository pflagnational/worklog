# == Schema Information
#
# Table name: leave_accrual_rules
#
#  id                            :integer          not null, primary key
#  accrual_type                  :string(255)
#  leave_code                    :string(255)
#  condition_type                :string(255)
#  condition_unit                :decimal(, )
#  unit                          :decimal(, )
#  reset_type                    :string(255)
#  reset_frequency               :string(255)
#  reset_date                    :date
#  reset_min                     :decimal(, )
#  reset_max                     :decimal(, )
#  reset_max_per_year_of_service :decimal(, )
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  accrual_frequency             :string(255)
#

class LeaveAccrualRule < ActiveRecord::Base
  attr_accessible :accrual_type, :condition_type, :condition_unit, :leave_code, :reset_date, :reset_frequency, :reset_max, :reset_max_per_year_of_service, :reset_min, :reset_type, :unit, :accrual_frequency 
    def check years_of_service
        op = self.condition_type
        a = years_of_service
        b = self.condition_unit
        case op
        when "<"
            a < b
        when "<="
            a <= b
        when ">"
            a > b
        when ">="
            a >= b
        else
            true
       end
        
    end
end
