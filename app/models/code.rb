# == Schema Information
#
# Table name: codes
#
#  id          :integer          not null, primary key
#  category    :string(255)
#  code        :string(255)
#  description :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  restriction :string(255)
#

class Code < ActiveRecord::Base
  attr_accessible :category, :code, :description, :restriction
  validates_uniqueness_of :code, :scope => :category

  scope :hour_types, where(category: ["Hours","Leave"])
  scope :paid_hour_types, hour_types.where("restriction not in (?) or restriction is NULL", ["Accrued Leave","Earned Leave"]) 
  scope :leave_types, where(category: "Leave")
  scope :earned_leave_types, where(restriction: "Earned Leave")
  scope :accrued_leave_types, where(restriction: "Accrued Leave")
  scope :leave_types_restricted_to_e, where(category: "Leave",code: "P")
    
  def self.categories
  	categories = []
  	Code.all.uniq_by{|x| x.category}.each do |c|
  		categories.push(c.category)
  	end
  	categories
  end

  def self.hour_types_by_restriction
    Code.hour_types.group_by { |s| (s.restriction unless s.restriction == "") || "General" }

  end

  def self.hour_types_by_restriction_for_select(admin = false)
    restriction_sorter = Proc.new { |x,y| 
      sort = 0
      if (x[0] == "General")
        sort = -1
      else
        sort = (x[0] <=> y[0])
      end
      sort
    }
    select = Code.uniq.pluck("Restriction").reject{|c| c == "Accrued Leave" && !admin}
      
    select.map! do |code|
    
      [(code unless code == "") || "General", Code.hour_types.find_all_by_restriction(code).map { |c| [c.description, c.code] }]
    end
    select.sort &restriction_sorter
  end

  def self.search(search)
  	if (search != "" && search != nil)
	  where(:category => search)
	else
		scoped
	end
  end


  def self.import(file)

    CSV.foreach(file.path, headers: true) do |row|

      if row['uploaded'].nil?
        row.push("uploaded" => Date.today()) 
      end

      row.delete_if do |k,v|
        true unless ['category','code','description'].include? k
      end

      code = Code.find_by_category_and_code(row["category"],row["code"]) || new
      code.attributes = row.to_hash
      code.save!
    end    
  end

end
