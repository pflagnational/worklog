# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20160129193305) do

  create_table "authentications", :force => true do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "codes", :force => true do |t|
    t.string   "category"
    t.string   "code"
    t.string   "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "restriction"
  end

  create_table "hours", :force => true do |t|
    t.string   "program"
    t.string   "activity"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "hourtype"
    t.string   "notes"
    t.integer  "work_log_id"
    t.decimal  "saturday_1"
    t.decimal  "sunday_1"
    t.decimal  "monday_1"
    t.decimal  "tuesday_1"
    t.decimal  "wednesday_1"
    t.decimal  "thursday_1"
    t.decimal  "friday_1"
    t.decimal  "saturday_2"
    t.decimal  "sunday_2"
    t.decimal  "monday_2"
    t.decimal  "tuesday_2"
    t.decimal  "wednesday_2"
    t.decimal  "thursday_2"
    t.decimal  "friday_2"
    t.decimal  "time"
  end

  create_table "leave_accrual_rules", :force => true do |t|
    t.string   "accrual_type"
    t.string   "leave_code"
    t.string   "condition_type"
    t.decimal  "condition_unit"
    t.decimal  "unit"
    t.string   "reset_type"
    t.string   "reset_frequency"
    t.date     "reset_date"
    t.decimal  "reset_min"
    t.decimal  "reset_max"
    t.decimal  "reset_max_per_year_of_service"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.string   "accrual_frequency"
  end

  create_table "leave_balances", :force => true do |t|
    t.string   "category"
    t.decimal  "balance"
    t.date     "uploaded"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "user_id"
  end

  create_table "leaves", :force => true do |t|
    t.string   "category"
    t.decimal  "balance"
    t.date     "uploaded"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "roles", ["name", "resource_type", "resource_id"], :name => "index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], :name => "index_roles_on_name"

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "users", :force => true do |t|
    t.string   "provider"
    t.string   "uid"
    t.string   "email"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.integer  "adp_id"
    t.integer  "department_id"
    t.string   "users"
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "supervisor_id"
    t.date     "hire_date"
    t.string   "accrual_type"
    t.integer  "alias_for_user_id", :default => -1
    t.string   "password_digest"
  end

  create_table "users_roles", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], :name => "index_users_roles_on_user_id_and_role_id"

  create_table "work_logs", :force => true do |t|
    t.integer  "user_id"
    t.date     "pay_period"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.integer  "submit_to"
    t.boolean  "approved"
    t.datetime "approved_at"
    t.integer  "approved_by_id"
    t.boolean  "template",       :default => false, :null => false
    t.datetime "processed"
  end

  add_index "work_logs", ["user_id"], :name => "index_work_logs_on_user_id"

end
