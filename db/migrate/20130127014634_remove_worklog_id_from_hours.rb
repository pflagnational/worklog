class RemoveWorklogIdFromHours < ActiveRecord::Migration
  def up
    remove_column :hours, :worklog_id
  end

  def down
    add_column :hours, :worklog_id, :string
  end
end
