class RemoveHoursIdFromWorkLog < ActiveRecord::Migration
  def up
    remove_column :work_logs, :hours_id
  end

  def down
    add_column :work_logs, :hours_id, :string
  end
end
