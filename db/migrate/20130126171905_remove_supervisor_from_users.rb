class RemoveSupervisorFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :supervisor
  end

  def down
    add_column :users, :supervisor, :string
  end
end
