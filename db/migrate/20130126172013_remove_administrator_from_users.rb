class RemoveAdministratorFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :administrator
  end

  def down
    add_column :users, :administrator, :string
  end
end
