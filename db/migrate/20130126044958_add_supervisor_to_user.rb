class AddSupervisorToUser < ActiveRecord::Migration
  def change
    add_column :users, :supervisor, :boolean
    add_column :users, :administrator, :boolean
  end
end
