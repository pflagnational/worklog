class RolifyCreateRoles < ActiveRecord::Migration
  def change
    unless table_exists? :roles
        create_table(:roles) do |t|
          t.string :name
          t.references :resource, :polymorphic => true

          t.timestamps
        end
    end

    unless table_exists? :users_roles
        create_table(:users_roles, :id => false) do |t|
          t.references :user
          t.references :role
        end

    add_index(:roles, :name)
    add_index(:roles, [ :name, :resource_type, :resource_id ])
    add_index(:users_roles, [ :user_id, :role_id ])
    end
  end
end
