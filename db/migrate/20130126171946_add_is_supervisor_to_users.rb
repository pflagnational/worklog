class AddIsSupervisorToUsers < ActiveRecord::Migration
  def change
    add_column :users, :is_supervisor, :boolean
    add_column :users, :is_administrator, :boolean
  end
end
