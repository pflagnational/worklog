class AddNotesToHours < ActiveRecord::Migration
  def change
    add_column :hours, :notes, :string
  end
end
