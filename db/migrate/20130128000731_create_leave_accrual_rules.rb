class CreateLeaveAccrualRules < ActiveRecord::Migration
  def change
    create_table :leave_accrual_rules do |t|
      t.string :accrual_type
      t.string :leave_code
      t.string :condition_type
      t.decimal :condition_unit
      t.decimal :unit
      t.string :reset_type
      t.string :reset_frequency
      t.date :reset_date
      t.decimal :reset_min
      t.decimal :reset_max
      t.decimal :reset_max_per_year_of_service

      t.timestamps
    end
  end
end
