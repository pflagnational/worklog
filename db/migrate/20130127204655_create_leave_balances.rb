class CreateLeaveBalances < ActiveRecord::Migration
  def change
    create_table :leave_balances do |t|
      t.string :category
      t.decimal :balance
      t.date :uploaded

      t.timestamps
    end
  end
end
