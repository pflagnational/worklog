class TemplateNotNullWorkLog < ActiveRecord::Migration
  def up
    change_column_default :work_logs, :template, false
    change_column_null :work_logs, :template, false
  end

  def down
    change_column_null :work_logs, :template, true
  end
end
