class CreateWorkLogs < ActiveRecord::Migration
  def change
    create_table :work_logs do |t|
      t.references :hour
      t.references :user
      t.date :pay_period

      t.timestamps
    end
    add_index :work_logs, :hour_id
    add_index :work_logs, :user_id
  end
end
