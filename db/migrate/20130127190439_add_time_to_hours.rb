class AddTimeToHours < ActiveRecord::Migration
  def change
    add_column :hours, :time, :integer
  end
end
