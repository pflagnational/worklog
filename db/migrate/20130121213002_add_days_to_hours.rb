class AddDaysToHours < ActiveRecord::Migration
  def change
    add_column :hours, :saturday_1, :integer
    add_column :hours, :sunday_1, :integer
    add_column :hours, :monday_1, :integer
    add_column :hours, :tuesday_1, :integer
    add_column :hours, :wednesday_1, :integer
    add_column :hours, :thursday_1, :integer
    add_column :hours, :friday_1, :integer
    add_column :hours, :saturday_2, :integer
    add_column :hours, :sunday_2, :integer
    add_column :hours, :monday_2, :integer
    add_column :hours, :tuesday_2, :integer
    add_column :hours, :wednesday_2, :integer
    add_column :hours, :thursday_2, :integer
    add_column :hours, :friday_2, :integer
  end
end
