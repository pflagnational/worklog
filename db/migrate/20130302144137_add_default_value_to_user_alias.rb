class AddDefaultValueToUserAlias < ActiveRecord::Migration
  def up
  	change_column :users, :alias_for_user_id, :integer, :default => -1
  end
  def down
  	change_column :users, :alias_for_user_id, :integer, default => nil
  end
end
