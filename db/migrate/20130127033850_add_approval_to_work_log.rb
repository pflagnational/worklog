class AddApprovalToWorkLog < ActiveRecord::Migration
  def change
    add_column :work_logs, :approved, :boolean
    add_column :work_logs, :approved_at, :datetime
    add_column :work_logs, :approved_by_id, :integer
  end
end
