class CreateHours < ActiveRecord::Migration
  def change
    create_table :hours do |t|
      t.string :type
      t.string :program
      t.string :activity
      t.decimal :time

      t.timestamps
    end
  end
end
