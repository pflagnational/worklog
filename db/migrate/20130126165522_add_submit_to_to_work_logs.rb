class AddSubmitToToWorkLogs < ActiveRecord::Migration
  def change
    add_column :work_logs, :submit_to, :integer
  end
end
