class CreateCodes < ActiveRecord::Migration
  def change
    create_table :codes do |t|
      t.string :category
      t.string :code
      t.string :description

      t.timestamps
    end
  end
end
