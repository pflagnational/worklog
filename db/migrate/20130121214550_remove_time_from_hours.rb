class RemoveTimeFromHours < ActiveRecord::Migration
  def up
    remove_column :hours, :time
  end

  def down
    add_column :hours, :time, :string
  end
end
