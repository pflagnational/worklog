class AddRestrictionToCode < ActiveRecord::Migration
  def change
    add_column :codes, :restriction, :string
  end
end
