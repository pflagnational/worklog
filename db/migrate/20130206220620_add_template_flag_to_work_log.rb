class AddTemplateFlagToWorkLog < ActiveRecord::Migration
  def change
    add_column :work_logs, :template, :boolean
  end
end
