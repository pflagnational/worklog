class RemoveRoleFlagsFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :is_supervisor
    remove_column :users, :is_administrator
  end

  def down
    add_column :users, :is_administrator, :string
    add_column :users, :is_supervisor, :string
  end
end
