class AddAccrualTypeToUser < ActiveRecord::Migration
  def change
    add_column :users, :accrual_type, :string
  end
end
