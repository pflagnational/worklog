class ChangeDataTypeForDaysHours < ActiveRecord::Migration
  def up
  	change_table :hours do |t|
  		t.change :saturday_1, :decimal
	    t.change :saturday_1, :decimal
	    t.change :sunday_1, :decimal
	    t.change :monday_1, :decimal
	    t.change :tuesday_1, :decimal
	    t.change :wednesday_1, :decimal
	    t.change :thursday_1, :decimal
	    t.change :friday_1, :decimal
	    t.change :saturday_2, :decimal
	    t.change :sunday_2, :decimal
	    t.change :monday_2, :decimal
	    t.change :tuesday_2, :decimal
	    t.change :wednesday_2, :decimal
	    t.change :thursday_2, :decimal
	    t.change :friday_2, :decimal  	
	end
  end

  def down
  	change_table :hours do |t|
	    t.change :saturday_1, :integer
	    t.change :sunday_1, :integer
	    t.change :monday_1, :integer
	    t.change :tuesday_1, :integer
	    t.change :wednesday_1, :integer
	    t.change :thursday_1, :integer
	    t.change :friday_1, :integer
	    t.change :saturday_2, :integer
	    t.change :sunday_2, :integer
	    t.change :monday_2, :integer
	    t.change :tuesday_2, :integer
	    t.change :wednesday_2, :integer
	    t.change :thursday_2, :integer
	    t.change :friday_2, :integer
	end
  end
end
