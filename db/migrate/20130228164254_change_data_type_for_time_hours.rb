class ChangeDataTypeForTimeHours < ActiveRecord::Migration
  def up
  	change_table :hours do |t|
	    t.change :time, :decimal
    end
  end

  def down
  	change_table :hours do |t|
	    t.change :time, :integer
    end
	end

end
