class AddProcessedTimeToWorkLog < ActiveRecord::Migration
  def change
    add_column :work_logs, :processed, :datetime
  end
end
