class RemoveTypeFromHours < ActiveRecord::Migration
  def up
    remove_column :hours, :type
  end

  def down
    add_column :hours, :type, :string
  end
end
