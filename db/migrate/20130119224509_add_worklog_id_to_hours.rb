class AddWorklogIdToHours < ActiveRecord::Migration
  def change
    add_column :hours, :worklog_id, :integer
  end
end
