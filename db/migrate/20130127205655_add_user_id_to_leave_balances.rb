class AddUserIdToLeaveBalances < ActiveRecord::Migration
  def change
    add_column :leave_balances, :user_id, :integer
  end
end
