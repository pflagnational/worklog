class AddAdpIdToUser < ActiveRecord::Migration
  def change
    add_column :users, :adp_id, :integer
  end
end
