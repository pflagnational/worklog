class AddAliasIdToUser < ActiveRecord::Migration
  def change
    add_column :users, :alias_for_user_id, :integer
  end
end
