class RemoveHourIdFromWorkLogs < ActiveRecord::Migration
  def up
    remove_column :work_logs, :hour_id
  end

  def down
    add_column :work_logs, :hour_id, :integer
  end
end
