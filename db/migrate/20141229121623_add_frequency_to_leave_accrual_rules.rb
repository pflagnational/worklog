class AddFrequencyToLeaveAccrualRules < ActiveRecord::Migration
  def change
    add_column :leave_accrual_rules, :accrual_frequency, :string
  end
end
